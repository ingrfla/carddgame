module idatt.ntnu.cardgame.cardgamee {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens idatt.ntnu.cardgame.cardgamee to javafx.fxml;
    exports idatt.ntnu.cardgame.cardgamee;
}